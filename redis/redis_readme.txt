启动：redis-server /usr/local/redis/redis.conf
重启：
清除当前数据库：flushdb
清除全部数据库的内容：flushall
redis查看启动目录：config get dir
一、string 
127.0.0.1:6379> mset k1 v1 k2 v2 k3 v3  #同时设置多个值
127.0.0.1:6379> mget k1 k2 k3   #同时获取多个值
127.0.0.1:6379> msetnx k1 v1 k4 v4  # msetnx 是一个原子性的操作，要么一起成功，要么一起
失败！
127.0.0.1:6379> EXISTS name  # 判断当前的key是否存在
(integer) 1
127.0.0.1:6379> EXISTS name1
(integer) 0
127.0.0.1:6379> move name 1  # 移除当前的key
(integer) 1
127.0.0.1:6379> EXPIRE name 10  # 设置key的过期时间，单位是秒
(integer) 1
127.0.0.1:6379> setex key3 30 "hello" # 设置key3 的值为 hello,30秒后过期
127.0.0.1:6379> ttl name  # 查看当前key的剩余时间
(integer) 4
127.0.0.1:6379> type name  # 查看当前key的一个类型！
string
127.0.0.1:6379> APPEND key1 "hello"  # 追加字符串，如果当前key不存在，就相当于setkey
(integer) 7
127.0.0.1:6379> STRLEN key1  # 获取字符串的长度！
(integer) 7
127.0.0.1:6379> incr views  # 自增1
(integer) 1
127.0.0.1:6379> decr views  # 自减1  
(integer) 1
127.0.0.1:6379> INCRBY views 10  # 可以设置步长，指定增量！
(integer) 11
127.0.0.1:6379> GETRANGE key1 0 3    # 截取字符串 [0,3]
127.0.0.1:6379> GETRANGE key1 0 -1 	 # 获取全部的字符串
27.0.0.1:6379> SETRANGE key2 1 xx  # 替换指定位置开始的字符串！
127.0.0.1:6379> setnx mykey "redis"   # 如果mykey 不存在，创建mykey
127.0.0.1:6379> setnx mykey "MongoDB"   # 如果mykey存在，创建失败！

二、list（在redis里面，我们可以把list玩成栈、队列、阻塞队列！）
127.0.0.1:6379> LPUSH list one two three  # 将一个值或者多个值，插入到列表头部 （左）
127.0.0.1:6379> Rpush list righr  # 将一个值或者多个值，插入到列表位部 （右）
127.0.0.1:6379> LRANGE list 0 -1   # 获取list中值！
127.0.0.1:6379> LRANGE list 0 1   # 通过区间获取具体的值！
127.0.0.1:6379> lindex list 1  # 通过下标获得 list 中的某一个值
127.0.0.1:6379> Lpop list  # 移除list的第一个元素
127.0.0.1:6379> Rpop list  # 移除list的最后一个元素
127.0.0.1:6379> Llen list  # 返回列表的长度
27.0.0.1:6379> lrem list 1 one  # 移除list集合中指定个数的value，精确匹配
127.0.0.1:6379> ltrim mylist 1 2   # 通过下标截取指定的长度，这个list已经被改变了，截断了
只剩下截取的元素
127.0.0.1:6379> rpoplpush mylist myotherlist  # 移除列表的最后一个元素，将他移动到新的
列表中！
127.0.0.1:6379> lset list 0 item   # 如果不存在列表我们去更新就会报错  如果存在 更新当前下标的值
(error) ERR no such key
127.0.0.1:6379> LINSERT mylist before world other    # 在列表中world前插入other
127.0.0.1:6379> LINSERT mylist after world new    # 在列表中world后插入new

三、set（集合）
set中的值是不能重读的！  （默认去重）
127.0.0.1:6379> sadd myset "hello"
127.0.0.1:6379> SMEMBERS myset     # 查看指定set的所有值
127.0.0.1:6379> SISMEMBER myset hello    # 判断某一个值是不是在set集合中
127.0.0.1:6379> scard myset  # 获取set集合中的内容元素个数
127.0.0.1:6379> srem myset hello  # 移除set集合中的指定元素
127.0.0.1:6379> SRANDMEMBER myset  # 随机抽选出一个元素
127.0.0.1:6379> SRANDMEMBER myset 2  # 随机抽选出n个的元素 
127.0.0.1:6379> spop myset  # 删除myset第一个元素
127.0.0.1:6379> smove myset myset2 "kuangshen" # 将一个指定的值，移动到另外一个set集
合
127.0.0.1:6379> SDIFF key1 key2    # 差集
127.0.0.1:6379> SINTER key1 key2   # 交集   共同好友就可以这样实现
127.0.0.1:6379> SUNION key1 key2   # 并集


四、Zset（有序集合）
在set的基础上，增加了一个值，set k1 v1 zset k1 score1 v1

127.0.0.1:6379> zadd myset 2 two 3 three   # 添加多个值
127.0.0.1:6379> ZRANGEBYSCORE myset -inf +inf  # 显示全部字段 从小到大
127.0.0.1:6379> ZRANGEBYSCORE myset -inf +inf withscores # 显示全部的字段并且附带值
127.0.0.1:6379> ZREVRANGE myset 0 -1 # 从大到小进行排序！
127.0.0.1:6379> ZRANGEBYSCORE myset -inf 3 withscores # 显示值小于3 升
序排序！
127.0.0.1:6379> zrem myset two   # 移除有序集合中的指定元素
127.0.0.1:6379> zcard myset  # 获取有序集合中的个数
127.0.0.1:6379> zcount myset 1 3  # 获取指定区间1-3的成员数量



五、Hash(哈希)
hash变更的数据 user name age,尤其是是用户信息之类的，经常变动的信息！ hash 更适合于对象的
存储，String更加适合字符串存储

127.0.0.1:6379> hset myhash field1 kuangshen  # set一个具体 key-vlaue
127.0.0.1:6379> hget myhash field1  # 获取一个字段值
127.0.0.1:6379> hmset myhash field1 hello field2 world   # set多个 key-vlaue
127.0.0.1:6379> hmget myhash field1 field2   # 获取多个字段值
127.0.0.1:6379> hgetall myhash   # 获取全部的数据
127.0.0.1:6379> hdel myhash field1  # 删除hash指定key字段！对应的value值也就消失了
127.0.0.1:6379> hlen myhash  # 获取hash表的字段数量
127.0.0.1:6379> HEXISTS myhash field1  # 判断hash中指定字段是否存在
127.0.0.1:6379> hkeys myhash  # 只获得所有field		（字段） 
127.0.0.1:6379> hvals myhash  # 只获得所有value		 （值）
127.0.0.1:6379> HINCRBY myhash field3 1		# 自增 步长为1
127.0.0.1:6379> hsetnx myhash field4 hello  # 如果不存在则可以设置
127.0.0.1:6379> hsetnx myhash field4 world  # 如果存在则不能设置




事务
Redis 事务本质：一组命令的集合！ 一个事务中的所有命令都会被序列化，在事务执行过程的中，会按
照顺序执行！
一次性、顺序性、排他性！执行一些列的命令
redis事务没有没有隔离级别的概念！
所有的命令在事务中，并没有直接被执行！只有发起执行命令的时候才会执行！Exec
Redis单条命令式保存原子性的，但是事务不保证原子性
redis的事务：开启事务（multi）-》命令入队-》执行事务（exec）

127.0.0.1:6379> multi    # 开启事务
127.0.0.1:6379> set k1 v1		#命令入队
QUEUED
127.0.0.1:6379> get k2
QUEUED	
。。。。。
127.0.0.1:6379> exec  # 执行事务	或者取消事务	127.0.0.1:6379> DISCARD   # 取消事务（事务队列中的命令不会被执行）
ok
如果队列中有错误命令  执行事务报错 所有的命令都不会被执行   如getset k3
如果事务队列存在语法性，其他命令可以正常执行，错误命令抛出异常    如：incr k1       k1的值为string则抛出异常   为integer则正常自增

悲观锁：很悲观，认为什么时候都会出问题，无论做什么都会加锁
乐观锁：很乐观，认为什么时候都不会出问题，所以不会上锁！ 更新数据的时候去判断一下，在此期间是否有人修改过这个数据
redis测监视测试  watch
测试多线程修改值 , 使用watch 可以当做redis的乐观锁操作
127.0.0.1:6379> set money 100
127.0.0.1:6379> watch money   # 监视  （上锁）
127.0.0.1:6379> multi
127.0.0.1:6379> DECRBY money 10
127.0.0.1:6379> INCRBY out 10
QUEUED
127.0.0.1:6379> exec  # 执行之前，另外一个线程，修改了我们的值，这个时候，就会导致事务执行失
败！
如果修改失败，获取最新的值就好
127.0.0.1:6379> unwatch			如果发现事务执行失败，解锁   获取最新的值  再监视



redis.conf配置
bind 127.0.0.1    # 绑定的ip
protected-mode yes # 保护模式
port 6379  # 端口设置
daemonize yes   # 以守护进程的方式运行，默认是 no，我们需要自己开启为yes！
pidfile /var/run/redis_6379.pid  # 如果以后台的方式运行，我们就需要指定一个 pid 文件
logfile "" # 日志的文件位置名
databases 16  # 数据库的数量，默认是 16 个数据库
always-show-logo yes  # 是否总是显示LOGO

快照（持久化，在规定事件内，执行了多少次操作，则会持久化到.rdb .aof）
1、save的规则满足的情况下，会自动触发rdb规则
2、执行 ﬂushall 命令，也会触发我们的rdb规则！
3、退出redis，也会产生 rdb 文件！
备份就自动生成一个 dump.rdb

# 如果900s内，如果至少有一个1 key进行了修改，我们及进行持久化操作
save 900 1
# 如果300s内，如果至少10 key进行了修改，我们及进行持久化操作
save 300 10
# 如果60s内，如果至少10000 key进行了修改，我们及进行持久化操作
save 60 10000
# 我们之后学习持久化，会自己定义这个测试！
stop-writes-on-bgsave-error yes   # 持久化如果出错，是否还需要继续工作！
rdbcompression yes # 是否压缩 rdb 文件，需要消耗一些cpu资源！
rdbchecksum yes # 保存rdb文件的时候，进行错误的检查校验！
dir ./  # rdb 文件保存的目录

AOF（Append Only File）
将我们的所有命令都记录下来，history，恢复的时候就把这个文件全部在执行一遍！
appendonly no    # 默认是不开启aof模式的，默认是使用rdb方式持久化的，在大部分所有的情况下，rdb完全够用！
appendfilename "appendonly.aof"  # 持久化的文件的名字
# appendfsync always   # 每次修改都会 sync。消耗性能
appendfsync everysec   # 每秒执行一次 sync，可能会丢失这1s的数据！
# appendfsync no       # 不执行 sync，这个时候操作系统自己同步数据，速度最快！
# rewrite		重写
优点：
1、每一次修改都同步，文件的完整会更加好！
2、每秒同步一次，可能会丢失一秒的数据
3、从不同步，效率最高的！
缺点：
1、相对于数据文件来说，aof远远大于 rdb，修复的速度也比 rdb慢！
2、Aof 运行效率也要比 rdb 慢，所以我们redis默认的配置就是rdb持久化





恢复rdb文件
1、只需要将rdb文件放在我们redis启动目录就可以，redis启动的时候会自动检查dump.rdb 恢复其中
的数据！
2、查看需要存在的位置
127.0.0.1:6379> config get dir
优点：
1、适合大规模的数据恢复！
2、对数据的完整性要不高！
缺点：
1、需要一定的时间间隔进程操作！如果redis意外宕机了，这个最后一次修改数据就没有的了！
2、fork进程的时候，会占用一定的内容空间！！





限制 clients
maxclients 10000   # 设置能连接上redis的最大客户端的数量
maxmemory <bytes>  # redis 配置最大的内存容量
maxmemory-policy noeviction  # 内存到达上限之后的处理策略
   1、volatile-lru：只对设置了过期时间的key进行LRU（默认值）
   2、allkeys-lru ： 删除lru算法的key  
   3、volatile-random：随机删除即将过期key  
   4、allkeys-random：随机删除  
   5、volatile-ttl ： 删除即将过期的  
   6、noeviction ： 永不过期，返回错误

APPEND ONLY 模式 aof配置
appendonly no    # 默认是不开启aof模式的，默认是使用rdb方式持久化的，在大部分所有的情况下，
rdb完全够用！
appendfilename "appendonly.aof"  # 持久化的文件的名字
# appendfsync always   # 每次修改都会 sync。消耗性能
appendfsync everysec   # 每秒执行一次 sync，可能会丢失这1s的数据！
# appendfsync no       # 不执行 sync，这个时候操作系统自己同步数据，速度最快





设置redis的密码 默认没有密码
127.0.0.1:6379> config get requirepass   # 获取redis的密码
1) "requirepass"
2) ""
127.0.0.1:6379> config set requirepass "123456"   # 设置redis的密码
OK
127.0.0.1:6379> config get requirepass   # 发现所有的命令都没有权限了
(error) NOAUTH Authentication required.
127.0.0.1:6379> ping
(error) NOAUTH Authentication required.
127.0.0.1:6379> auth 123456  # 使用密码进行登录！
OK
127.0.0.1:6379> config get requirepass
1) "requirepass"
2) "123456" 



三种特殊数据类型
Geospatial地理位置
127.0.0.1:6379> geoadd china:city 106.50 29.53 chongqi 114.05 22.52 shengzhen     添加地理位置

127.0.0.1:6379> GEOPOS china:city beijing  # 获取指定的城市的经度和纬度

127.0.0.1:6379> GEODIST china:city beijing shanghai km  # 查看上海到北京的直线距离 以km为单位

127.0.0.1:6379> GEORADIUS china:city 110 30 1000 km  # 以110，30 这个经纬度为中心，寻找方圆(半径)1000km内的城市
127.0.0.1:6379> GEORADIUS china:city 110 30 500 km withdist  # 显示到中间距离的位置
127.0.0.1:6379> GEORADIUS china:city 110 30 500 km withcoord  # 显示他人的定位信息
127.0.0.1:6379> GEORADIUS china:city 110 30 500 km withdist withcoord count 1  #筛选出指定的结果

127.0.0.1:6379> GEORADIUSBYMEMBER china:city beijing 1000 km		# 找出位于指定元素周围的其他元素

127.0.0.1:6379> geohash china:city beijing chongqi			# 将二维的经纬度转换为一维的字符串，如果两个字符串越接近，那么则距离越近

GEO 底层的实现原理其实就是 Zset！我们可以使用Zset命令来操作geo
127.0.0.1:6379> ZRANGE china:city 0 -1  # 查看地图中全部的元素
127.0.0.1:6379> zrem china:city beijing  # 移除指定元素




Hyperloglog
优点：占用的内存是固定，2^64 不同的元素的技术，只需要废 12KB内存！如果要从内存角度来比较的
话 Hyperloglog 首选
网页的 UV （一个人访问一个网站多次，但是还是算作一个人，统计访问网站次数）如果允许容错，那么一定可以使用 Hyperloglog 
																		  如果不允许容错，就使用 set 或者自己的数据类型即可
127.0.0.1:6379> PFadd mykey a b c d e f g h i j   # 创建第一组元素 mykey
127.0.0.1:6379> PFCOUNT mykey  # 统计 mykey 元素的基数数量
127.0.0.1:6379> PFadd mykey2 i j z x c v b n m   # 创建第二组元素 mykey2
127.0.0.1:6379> PFMERGE mykey3 mykey mykey2  # 合并两组 mykey mykey2 => mykey3 并集



Bitmap(位存储、位图)
统计用户信息，活跃，不活跃！ 登录 、 未登录！ 打卡，365打卡！ 两个状态的，都可以使用
127.0.0.1:6379> setbit sign 0 1      #记录打开    0表星期日  1表已打卡
127.0.0.1:6379> getbit sign 0		#查看是否打卡
127.0.0.1:6379> bitcount sign  # 统计这周的打卡记录，就可以看到是否有全勤



redis主从
默认情况下，每台Redis服务器都是主节点； 我们一般情况下只用配置从机就好
从机
127.0.0.1:6380> SLAVEOF 127.0.0.1 6379   #SLAVEOF host 6379   找谁当自己的老大
127.0.0.1:6380> info replication	#查看本机信息
# Replication
role:slave  # 当前角色是从机
master_host:127.0.0.1   # 可以的看到主机的信息
master_port:6379
主机
127.0.0.1:6379> info replication
# Replication
role:master		# 当前角色是主机
connected_slaves:1  # 多了从机的配置
slave0:ip=127.0.0.1,port=6380,state=online,offset=42,lag=1    # 多了从机的配置

主机可以写，从机不能写只能读！主机中的所有信息和数据，都会自动被从机保存
复制原理：
Slave 启动成功连接到 master 后会发送一个sync同步命令
Master 接到命令，启动后台的存盘进程，同时收集所有接收到的用于修改数据集命令，在后台进程执行
完毕之后，master将传送整个数据文件到slave，并完成一次完全同步。
全量复制：而slave服务在接收到数据库文件数据后，将其存盘并加载到内存中。
增量复制：Master 继续将新的所有收集到的修改命令依次传给slave，完成同步
但是只要是重新连接master，一次完全同步（全量复制）将被自动执行！ 我们的数据一定可以在从机中看到！

如果主机断开连接   可以使用slaveof no noe 让自己变成主机，其他的节点就可以手动连接到最新的这个主节点（手动）！如果这个时候老大修复了，那就重新连接

哨兵模式（自动选举老大的模式）
主从切换技术的方法是：当主服务器宕机后，需要手动把一台从服务器切换为主服务器，这就需要人工干预，费事费力，还会造成一段时间内服务不可用。这不是一种推荐的方式，更多时候，我们优先考虑哨兵模式
哨兵模式是一种特殊的模式，首先Redis提供了哨兵的命令，哨兵是一个独立的进程，作为进程，它会独立运行。其原理是哨兵通过发送命令，等待Redis服务器响应，从而监控运行的多个Redis实例
这里的哨兵有两个作用：
	1.通过发送命令，让Redis服务器返回监控其运行状态，包括主服务器和从服务器。
	2.当哨兵监测到master宕机，会自动将slave切换成master，然后通过发布订阅模式通知其他的从服
	务器，修改配置文件，让它们切换主机。
然而一个哨兵进程对Redis服务器进行监控，可能会出现问题，为此，我们可以使用多个哨兵进行监控。各个哨兵之间还会进行监控，这样就形成了多哨兵模式
优点：
1、哨兵集群，基于主从复制模式，所有的主从配置优点，它全有
2、 主从可以切换，故障可以转移，系统的可用性就会更好
3、哨兵模式就是主从模式的升级，手动到自动，更加健壮！
缺点：
1、Redis 不好啊在线扩容的，集群容量一旦到达上限，在线扩容就十分麻烦！
2、实现哨兵模式的配置其实是很麻烦的，里面有很多选择！

哨兵模式的全部配置！
# Example sentinel.conf
# 哨兵sentinel实例运行的端口 默认26379		
port 26379
# 哨兵sentinel的工作目录	
dir /tmp
# 哨兵sentinel监控的redis主节点的 ip port
# master-name
可以自己命名的主节点名字 只能由字母A-z、数字0-9 、这三个字符".-_"组成。
 
# quorum 配置多少个sentinel哨兵统一认为master主节点失联 那么这时客观上认为主节点失联了
# sentinel monitor <master-name> <ip> <redis-port> <quorum>
sentinel monitor mymaster 127.0.0.1 6379 2

# 当在Redis实例中开启了requirepass foobared 授权密码 这样所有连接Redis实例的客户端都要提供
密码
# 设置哨兵sentinel 连接主从的密码 注意必须为主从设置一样的验证密码
# sentinel auth-pass <master-name> <password>
sentinel auth-pass mymaster MySUPER--secret-0123passw0rd

# 指定多少毫秒之后 主节点没有应答哨兵sentinel 此时 哨兵主观上认为主节点下线 默认30秒
# sentinel down-after-milliseconds <master-name> <milliseconds>
sentinel down-after-milliseconds mymaster 30000

# 这个配置项指定了在发生failover主备切换时最多可以有多少个slave同时对新的master进行 同步，
这个数字越小，完成failover所需的时间就越长，
但是如果这个数字越大，就意味着越 多的slave因为replication而不可用。
可以通过将这个值设为 1 来保证每次只有一个slave 处于不能处理命令请求的状态。
# sentinel parallel-syncs <master-name> <numslaves>
sentinel parallel-syncs mymaster 1

# 故障转移的超时时间 failover-timeout 可以用在以下这些方面：
#1. 同一个sentinel对同一个master两次failover之间的间隔时间。
#2. 当一个slave从一个错误的master那里同步数据开始计算时间。直到slave被纠正为向正确的master那
里同步数据时。
#3.当想要取消一个正在进行的failover所需要的时间。  
#4.当进行failover时，配置所有slaves指向新的master所需的最大时间。不过，即使过了这个超时，
slaves依然会被正确配置为指向master，但是就不按parallel-syncs所配置的规则来了
# 默认三分钟
# sentinel failover-timeout <master-name> <milliseconds>
sentinel failover-timeout mymaster 180000

# SCRIPTS EXECUTION
#配置当某一事件发生时所需要执行的脚本，可以通过脚本来通知管理员，例如当系统运行不正常时发邮件通知
相关人员。
#对于脚本的运行结果有以下规则：
#若脚本执行后返回1，那么该脚本稍后将会被再次执行，重复次数目前默认为10
#若脚本执行后返回2，或者比2更高的一个返回值，脚本将不会重复执行。
#如果脚本在执行过程中由于收到系统中断信号被终止了，则同返回值为1时的行为相同。
#一个脚本的最大执行时间为60s，如果超过这个时间，脚本将会被一个SIGKILL信号终止，之后重新执行。

#通知型脚本:当sentinel有任何警告级别的事件发生时（比如说redis实例的主观失效和客观失效等等），
将会去调用这个脚本，这时这个脚本应该通过邮件，SMS等方式去通知系统管理员关于系统不正常运行的信
息。调用该脚本时，将传给脚本两个参数，一个是事件的类型，一个是事件的描述。如果sentinel.conf配
置文件中配置了这个脚本路径，那么必须保证这个脚本存在于这个路径，并且是可执行的，否则sentinel无
法正常启动成功。
#通知脚本
# shell编程
# sentinel notification-script <master-name> <script-path>
sentinel notification-script mymaster /var/redis/notify.sh

# 客户端重新配置主节点参数脚本
# 当一个master由于failover而发生改变时，这个脚本将会被调用，通知相关的客户端关于master地址已
经发生改变的信息。
# 以下参数将会在调用脚本时传给脚本:
# <master-name> <role> <state> <from-ip> <from-port> <to-ip> <to-port>
# 目前<state>总是“failover”,
# <role>是“leader”或者“observer”中的一个。
# 参数 from-ip, from-port, to-ip, to-port是用来和旧的master和新的master(即旧的slave)通
信的
# 这个脚本应该是通用的，能被多次调用，不是针对性的。
# sentinel client-reconfig-script <master-name> <script-path>
sentinel client-reconfig-script mymaster /var/redis/reconfig.sh # 一般都是由运维来配
置！






扩展：
1、RDB 持久化方式能够在指定的时间间隔内对你的数据进行快照存储
2、AOF 持久化方式记录每次对服务器写的操作，当服务器重启的时候会重新执行这些命令来恢复原始
的数据，AOF命令以Redis 协议追加保存每次写的操作到文件末尾，Redis还能对AOF文件进行后台重
写，使得AOF文件的体积不至于过大。
3、只做缓存，如果你只希望你的数据在服务器运行的时候存在，你也可以不使用任何持久化
4、同时开启两种持久化方式
在这种情况下，当redis重启的时候会优先载入AOF文件来恢复原始的数据，因为在通常情况下AOF
文件保存的数据集要比RDB文件保存的数据集要完整。
RDB 的数据不实时，同时使用两者时服务器重启也只会找AOF文件，那要不要只使用AOF呢？作者
建议不要，因为RDB更适合用于备份数据库（AOF在不断变化不好备份），快速重启，而且不会有
AOF可能潜在的Bug，留着作为一个万一的手段。
5、性能建议
因为RDB文件只用作后备用途，建议只在Slave上持久化RDB文件，而且只要15分钟备份一次就够
了，只保留 save 900 1 这条规则。
如果Enable AOF ，好处是在最恶劣情况下也只会丢失不超过两秒数据，启动脚本较简单只load自
己的AOF文件就可以了，代价一是带来了持续的IO，二是AOF rewrite 的最后将 rewrite 过程中产
生的新数据写到新文件造成的阻塞几乎是不可避免的。只要硬盘许可，应该尽量减少AOF rewrite
的频率，AOF重写的基础大小默认值64M太小了，可以设到5G以上，默认超过原大小100%大小重
写可以改到适当的数值。
如果不Enable AOF ，仅靠 Master-Slave Repllcation 实现高可用性也可以，能省掉一大笔IO，也
减少了rewrite时带来的系统波动。代价是如果Master/Slave 同时倒掉，会丢失十几分钟的数据，
启动脚本也要比较两个 Master/Slave 中的 RDB文件，载入较新的那个，微博就是这种架构
