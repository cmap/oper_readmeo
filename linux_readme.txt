﻿Linux命令

df -h  查看磁盘分区上的磁盘空间
du -sh 查看文件和目录大小

uame -a    内核信息
lsb_release -a 		系统信息
hostname	检视你的机器的主机名
time <cmd>		执行一个命令并报告用时
du -ha <filename>		在磁盘上显示目录及其内容的大小

③重定向
命令 > 目的文件
ls -l > a.txt
将当前目录详细信息输出到文件a.txt

④管道
ls --help | more
ls --help | less
将一个程序或者命令的输出作为另一个程序的输入

4. ls tree clear
ls列出文件或文件夹
tree以树状列出文件或文件夹
ls -l -a -h
tree -L 层数
tree -L 3
tree -LhaC 3  向下展示三级目录结构（带有文件大小信息和隐藏目录信息）

clear 	清屏

pwd		显示当前目录详细信息

6.cat rm cp mv
cat a.txt	显示文本文件内容
rm a.txt //删除文件
rm -r 文件夹名  //删除文件夹
rm -f //force,强制删除
rm -rf 文件夹或文件

cp -r   拷贝文件夹
cp a.txt ./mutex/
cp -v 显示拷贝进度
mv：move移动，剪切
mv a.txt b.txt重命名
mv a.txt ./mutex/aaa.txt
mv a.txt ./mutex/

7.mkdir rmdir touch
mkdir xxx	 创建文件夹
mkdir -p xxx/uuu/kkk以递归的形式建立文件夹
rmdir xxx	删除空的目录
touch aa.txt	创建一个文件


8.find grep (查找)
find：在指定的路径下去找文件
find aaa.txt 只在当前路径下找文件
find ./share/ -name aaa.txt 在指定路径下找文件
find  -name aaa.txt 在当前路径下所有的目录去找文件
grep 在指定文件中找内容
grep 查找内容 文件名 -n参数 显示行号
grep TCP-recv.c aaa.txt -n
grep -inr <查找的内容>		 在当前目录或子目录的文件中搜索一个字符串   or grep -r <字符串>
grep -rl <查找的内容>	只显示文件名

grep -rn "***" ./             在当前路径下搜索 （-r显示路径  n显示行号）
	-v	(反向匹配)
	-i	(不区分大小写)
	-c	(统计出现的次数)
	-l	(显示文件名)
	-e	简单的正则表达式
	-E	复杂的正则表达式	grep -E '(foo|bar)baz' file.txt

sed (编辑处理)
14.sed -i "s/原字符串/新字符串/g" <filename>       替代文件中的一个字符串
有无g的区别：
	没有g：如果一行中有多个原字符串，那么只替换第一个 
	有g：  如果一行中有多个原字符串，那么全部替换
sed "参数"							sed '$a123abc' 123.txt   向123.txt文件最后一行添加123abc
a:追加	向匹配行后面插入内容    如：sed '1a123abc' 123.txt   向123.txt文件第一行后面添加123abc     
i:插入	向匹配行前插入内容		如：sed '1i321cba' 123.txt   向123.txt文件第一行前面添加321cba 
c:更改	更改匹配行的内容		如：sed '1c3xx3xx' 123.txt   更改123.txt文件第一行内容为3xx3xx	 (整行)
d:删除	删除匹配行的内容		如：sed '1d' 123.txt	删除第一行内容							
s:替换	替换掉匹配的内容		如:	sed "s/原字符串/新字符串/" <filename> 						 (字符串)


awk（分割处理）
命令格式：awk [选项] '脚本命令' 文件名
	常用选项： 
		-F：指定以fs作为输入行的分隔符，默认为空
		-f：从脚本文件中读取awk脚本命令，以取代直接在命令行中输入指令
		-v：在执行处理过程之前，设置一个变量var，并给其设备的初始值为val（-v var=val）
	常用脚本变量：
		FIELDWIDTHS:由空格分割的一列数字，定义了每个数据字段的确切宽度
		FNR:当前输入文档的记录编号，常在由多个输入文档时使用
		NR:输入流的当前记录编号  (NR、NF分别为行号和列数)
		FS:输入字段分隔符
		RS:输入记录分隔符，默认为\n
		OFS:输出字段分隔符，默认为空格
		ORS:输出字段分隔符，默认为\n
eg:
	显示/etc/passwd的第1列和第7列，用逗号分隔显示，所有行开始前添加列名start1，start7，最后一行添加，end1，end7
	awk -F ':' 'BEGIN {print "start1,start7"} {print $1 "," $7} END {print "end1,end7"}' /etc/passwd 
	
按时间顺序删除指定目录文件
ls ./pic/ -lrt| awk '{print "./pic/" $9}'| head -n 10 | tail -n 9 | xargs rm -rf		（head -n 前几行  tail -n 后几行）

cut（分割处理，跟awk类似）
-d 定义分隔符，默认位tab键
-f 表示需要取得哪个字段，2表示取得以-d指定分隔符分割的第二个字段
eg：获取ip地址
ifconfig ens33 | awk "/inet addr/{print}" | cut -f 2 -d ":" | cut -f 1 -d " "

cut切割命令（从文件或标准输入中提取字段并输出到标准输出）
-f   打印文本的列     
-d 	 分隔符
1. echo $(($RANDOM%5+3)) | md5sum | cut -c 1-8
	生成随机数	$RANDOM       	#0-32767
	如想得到6-87范围内的数：$(($RANDOM%82+6))
	cut 切割命令（得出的md5值切割显示出前8位）
echo 192.168.1.1 | cut -d. -f1           cut -d. 以.为分隔符   -f1:取第一个

文件切割
split文件切割（将大文件切割成多个较小的文件）
split -l 100 file.txt	#指定每个切割文件的行数， “-l 100” 表示每个文件有100行
split -b 1M file.txt	#指定每个切割文件的大小，“-b 1M”表示每个文件有1MB

dd命令
dd命令是一种用于转换和复制文件的工具，也可以用于切割文件。
# if参数指定输入文件名，of为输出文件名，bs参数指定每个块的大小，count参数指定要复制的块数
dd if=文件名 of=输出文件 bs=块大小 count=块数		
dd if=file.txt of=output bs=1M count=1   	# 生成一个名为“output”的文件，其中包含输入文件的前1MB数据
	

软硬链接区别
软连接：类似于创建一个快捷方式。     源文件删除 软连接无效，不可访问   
硬连接：把文件的名称映射到文件的实际位置。	源文件删除，硬连接仍有效
9.ln：创建软链接文件
ln -s 源文件名字   连接文件（快捷方式）的名字 
ln -s test.txt test2.txt
注：test2.txt 就是test.txt 连接文件（快捷方式）	

10.tar：压缩与解压缩命令
gzip格式：
1.压包（打包）
	压缩用法： tar zcvf 压缩包包名   文件1  文件2 ...
	tar  zcvf  bk.tar.gz   a.c b.txt xiutao test.txt
	将a.c b.txt xiutao  test.txt  文件或文件夹压缩到 bk.tar.gz中
	
tar：打包加密
	tar -zcf - 要加密的文件或目录 |openssl des3 -salt -k 加密的密码 | dd of=加密后的文件
tar：解压解密
	dd if=压缩包文件 |openssl des3 -d -k 密码|tar zxf -
	
	zip打包加密码： zip -rP 123456 a.tar.gz a.txt
	
2.解压
	解压方法1：tar zxvf  压缩包的名称  
	解压到当前目录
	tar zxvf bk.tar.gz			  
	解压方法2：tar zxvf  压缩包的名称  -C  目的路径
	解压到指定路径下
	tar zxvf bk.tar.gz  -C  /home/teacher/share
	将压缩包里的文件或文件夹 解压到/home/teacher/share目录下		
	注意：gzip格式的压缩包后缀名.tar.gz结尾			
bz2格式：
	1.压包
	tar  jcvf  bk.tar.bz2   a.c b.txt xiutao test.txt	
	2.解压
	tar  jxvf bk.tar.bz2  -C  /home/teacher/share	 指定路径
	
	
11.进程相关
查看当前进程
ps 当前进程
ps -e -l详细进程信息
杀死进程
kill
kill -9 进程号
kill -KILL 进程号 强制杀死进程			-9 -KILL效果一样


12. wc命令用于计算字数。
利用wc指令我们可以计算文件的Byte数、字数、或是列数，若不指定文件名称、或是所给予的文件名为"-"，则wc指令会从标准输入设备读取数据。

语法
wc [-clw][--help][--version][文件...]
参数：

-c或--bytes或--chars 只显示Bytes数。
-l或--lines 只显示行数。
-w或--words 只显示字数。
--help 在线帮助。
--version 显示版本信息

cat file.txt | xargs -n1 | sort | uniq -c 		统计文件中的独特字（unique words）数量   xargs -n2:以2列显示   

xargs用法    (xargs默认分隔符是空格|回车)
find示例：删除文件	
	find . -name "123.txt" -type f | xargs rm -rf {}	查找当前目录下类型为文件的123.txt文件    xargs起连接作用    {}：指前面查找出来的文件
grep示例：复制文件
	grep -rl "查找字符串的内容" | xargs -i cp {} /data/	  -rl：只显示文件名   xargs -i：分隔符为Tab（如查找出多个文件，遍历复制文件）		


13.top/htop 任务管理器


Linux系统预留可三个文件描述符：0、1和2，他们的意义如下所示：
0——标准输入（stdin）
1——标准输出（stdout）
2——标准错误（stderr）	


du -b 是文件实际的大小
du -k 是文件占用空间的大小		find . -name "*".html -exec du -k {} \; 统计当前目录下.html文件占内存的大小

du -h 查看系统磁盘大小
df -h 显示文件或目录大小



shell脚本

shell 脚本 单引号不会对变量进行解析
元字符：# $ 空格

unset 删除变量/数组

$0	当前shell程序的名称
$#	传递给shell程序的位置参数的数量
$*	调用shell程序时所传送的全部参数组成的单字符串
$?	前一个命令或函数的返回值
$$	本程序的PID（进程的ID号）
$!	上一个命令的PID

文件判断
-d	确定文件是否为目录
-e	确定文件是否存在
-f	确定文件是否为普通文件
-s	确定文件大小是否为非0，则为真

整数判断
-eq	比较两个整数是否相等
-ne	比较两个整数是否不等
-ge	比较一个整数是否大于等于另一个整数
-gt	比较一个整数是否大于另一个整数
-le	比较一个整数是否小于等于另一个整数
-lt	比较一个整数是否小于另一个整数

字符串判断
=~ 	正则匹配  if [[ $1 =~ ^[0-9]+$ ]]; then   #判断是否为数字
==	比较两个字符串是否相等
!=	比较两个字符串是否不相等
-n	判断字符串长度是否大于零
-z	判断字符串长度是否等于零

默认shell变量是弱类型变量，字符串类型，可使用declare声明变量类型
declare [+/-] [选项] 变量名
+：取消变量类型的属性
-：给变量设定类型属性
选项类型：
a:将变量声明为数组型
i:将变量声明为整型
x:将变量声明为环境变量
r:将变量声明为只读变量
p:显示指定变量的被声明的类型


数组：
a=(1,2,'fdfsfdsf')
b=([1]=3,[4]=55,[6]=11)
1.使用@或*获取数组中所有元素：
${a[*]}
${b[@]}
2.获取数组的长度：${#a[*]}
3.获取数组的下标值：${!a[@]}
循环中$*和$@的区别：（循环中推荐使用$(a[@])）
	$(a[*])会将数组元素视为一个整体
	$(a[@])会将数组元素是为单独的个体
	


wget和curl的区别
wget:是一个无需依赖其他扩展的独立、简单的程序，专注于下载且支持递归下载，如果遇到访问链接301、302等情况会自动进行跳转然后进行下载
eg：wget -T 10 --tries=1 --spider www.baidu.com		#-T超时时间，--tries尝试1次，--spider爬虫模式
curl:是一个功能完备的网络请求工具，从stdin读取数据将网络请求结果输出到stdout。{并发测试、下载数据、上传数据、获取响应报头进行分析}