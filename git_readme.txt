git blame *.py  查看最近一笔改动

1.右击要管理的文件夹->Git Bash Here
git config --global user.name "用户名"
git config --global user.email "邮箱"
2.git init		把这个文件夹变成Git可管理的仓库（.git）    使用git仓库
3.git status	查看你文件的当前状态	git status -s 	状态详细
4.git add .	把目录下的所有文件添加到仓库	添加到暂缓区
5.git commit -m "***"	把项目提交到仓库(-m后面的引号是本次提交的注释内容。可写可不写)

不连续commit合并
https://blog.csdn.net/qq_29518275/article/details/122052289


只展示文件名
git show --pretty='format:' --name-only [commitid]


查看提交历史		
git log	推荐用 git log --pretty=oneline（可以指定使用不同于默认格式的方式展示提交历史）
git log --pretty=format:"%h - %an, %ar : %s"(指定格式展示提交历史)
git log --pretty=format:"%h %s"	显示 ASCII 图形表示的分支合并历史
git log -p    用于显示每次提交的内容差异     eg: git log -p -2      显示最近两次提交的内容差异  git log --since=2.weeks 两周内提交的内容差异

git log --stat  	显示每次更新的文件修改统计信息
git log --author ygc		显示指定作者相关的提交




版本回退
回退上一个版本		git reset --hard HEAD^
回退上上个版本		git reset --hard HEAD^^
指定版本			git reset --hard 21a77	21a77版本号前5位




撤销操作(如发现你提交到本地仓库的代码有问题或者要修改)  git commit --amend -m ""			
eg: 
git add a.txt
git commit -m "a"
vim a.txt
git add a.txt
git commit --amend -m "a_update"          此次提交将会替换之前提交的内容，暂存区的快照不变




删除文件
1：如果说一文件已经提交到本地库里
使用git rm test.txt之后再git commit -m "remve test.txt"
还原文件		git checkout -test.txt
2：如果说一文件已经在暂缓区中，则
使用git rm --cached test.txt 



忽略文件
创建一个名为 .gitignore的文件，列出要忽略的文件模式。 eg: .[oa] 忽略所有.o或.a结尾的文件



重命名文件
git mv test.txt newtest.txt






查看已暂存和未暂存的修改
git diff
查看已暂存的将要添加到下次提交里的内容
git diff --cached

取消暂存的文件
git reset HEAD <file>

撤销对文件的修改
git checkout -- <file>



查看远程仓库	git remote -v
添加远程仓库	git remote add <shortname> <url>	之后拉取远程仓库：git fetch <shortname> 即可代替<url>
远程仓库重命名rename	git remote rename oldname newname
远程仓库移除	git remote rm <originname>



连接远程仓库（github）
 由于本地Git仓库和Github仓库之间的传输是通过SSH加密的，所以连接时需要设置一下：
1.创建SSH KEY。 先看一下你C盘用户目录下有没有.ssh目录，有的话看下里面有没有id_rsa和id_rsa.pub这两个文件，有就跳到下一步，没有就通过下面命令创建
	ssh-keygen -t rsa -c "邮箱地址"
   然后一路回车。这时你就会在用户下的.ssh目录里找到id_rsa和id_rsa.pub这两个文件
2.登录Github,找到右上角的图标，打开点进里面的Settings，再选中里面的SSH and GPG KEYS，点击右上角的New SSH key，然后Title里面随便填，再把刚才id_rsa.pub里面的内容复制到Title下面的Key内容框里面，最后点击Add SSH key，这样就完成了SSH Key的加密。
3.在Github上创建一个Git仓库。直接点New repository来创建。
4*.在Github上创建好Git仓库之后我们就可以和本地仓库进行关联了，根据创建好的Git仓库页面的提示，可以在本地仓库的命令行输入：
  git remote add origin (New repository里的url)	如：git remote add origin https://gitee.com/cmap/ttsx.git
5.关联好之后我们就可以把本地库的所有内容推送到远程仓库（也就是Github）上了，通过
   git push -u origin master		(首次需要 因为新建的远程仓库是空的，之后就不用了 直接下面的命令)
   git push origin master

6.从远程仓库下载到服务器或本地：
git pull origin master

*git pull拉取不成功解决：
由于本地的更改所以没有拉取成功，可以这样解决：
方法一：git stash 将本地修改储存起来，然后再Git pull 就可以了呢
方法二：git clean -d -fx  清除本地仓库相同文件名    再Git pull 


error
git添加远程库报错        解决：git remote rm origin 删除本地连接的远程库




打标签
列出标签  git tag     或者   git tag -l 'v1.*'
1.轻量级标签
git tag v1.4-lw
2.附注标签（存储在 Git 数据库中的一个完整对象（包括打标签者，标签信息，日期时间））
打上标签：git tag -a v1.4 -m 'my version 1.4'
查看标签：git show v1.4

后期打标签
git tag -a v1.4 <校验和（8305f3e）> -m 'my version 1.4'

共享标签：git push origin v1.4           或者 git push origin --tags 标签全部传送

检出标签（如果你想要工作目录与仓库中特定的标签版本完全一样，可以使用）
git checkout -b version2 v2.0.0.0





git分支
分支创建：git branch testing
分支创建并同时切换到分支：git checkout -b testing
分支切换：git checkout testing
查看各个分支当前所指的对象	git log --oneline --decorate
查看分支分叉历史：git log --oneline --decorate --graph --all
分支合并：
git merge testing 	
情况一：(master分支是testing分支的直接祖先--》称为快进，表示master指向testing同时指向校验和)
情况二：(master分支不是testing分支的直接祖先--》Git 会使用两个分支的末端所指的快照（C4 和 C5
）以及这两个分支的工作祖先（C2），做一个简单的三方合并。结果做了一个新的快照并且自动创建一个新的提
交指向它。 这个被称作一次合并提交，它的特别之处在于他有不止一个父提交)   称为三方合并
情况三：可采用变基：先切换到分支testing：git checkout testing        git rebase master  称为在master分支上重放    再回到master分支，进行一次快速合并 git merge testing

冲突合并
查看状态  git status    ----》手动合并

查看已合并分支	git branch --merged
查看未合并分支	git branch --no-merged

删除分支		git branch -d testing

删除远程分支serverfix		git push origin --delete serverfix



git checkout -- 文件  （撤销对文件的修改）
git reset HEAD或版本号   （取消暂存的版本号）
git reset HEAD <file>   (取消暂存的文件)
git reflog   （查看所有分支的所有操作记录（包括已经被删除的 commit 记录和 reset 的操作））
git branch --set-upstream-to=origin/master master    （追踪远程分支origin/master分支到本地master分支，本地与远程分支进行关联，就可以进行git pull  push操作）
git checkout -b dev origin/dev   (切换到dev分支上,接着跟远程的origin地址上的dev分支关联起来，也可以理解为从远程拉取分支到本地)
git diff 版本1 版本2   (对比两个版本有何差异，显示出来)
git merge 分支名称    （合并分支）
git pull     （从远程获取最新版本并合并本地版本）
git push origin 分支名称      （推送到远程分支）



git stash    （临时保存和回复修改：注意：必须在没有add加入缓存区前才能stash）
git stash list
git stash apply 
git stash clear    (删除所有保存)
git stash drop stash@{0}
git stash save -u ""		同时保存新增的文件


git branch -vv     (查看本地分支与远程分支的映射关系)
git branch --unset-upstream     (撤销本地分支与远程分支的关系)




https://blog.csdn.net/litongqiang/article/details/107388918





天地和兴
1.创建一个本地test分支跟远端基线分支develop同步：git checkout -b test origin/develop
2.将test分支push到远端：git push origin/test
3.检查远端是否有你的test分支：git branch -r 或者git branch origin/te   Tab补齐
4. 配置远程仓库信息,将对应的远端分支改为自己的分支： vi .git/config   （merge=refs/heads/test）

如果前端更新了远程分支的code，导致无法push
1.git merge origin/远程分支
2.git pull    
3.git push

查看commit属于哪个分支：git branch -r --comtains [COMMIT_ID]

最后
git pull --rebase
git merge origin/develop  ->解冲突 ->add->commit -m "IMAP-1319 merge develop"->push
 


git branch -D ygc_test    删除分支及相关的远程仓库分支
git push --set-upstream origin ygc_test 本地关联远程分支




删除本地分支：git branch -d 分支名（remotes/origin/分支名）
强制删本地：git branch -D 分支名
删除远程分支：git push origin --delete 分支名（remotes/origin/分支名）


回退到远程分支版本
git reset --hard origin/imap_v2.7_rbug

git reset --hard origin/imap_vul_table_merge




	