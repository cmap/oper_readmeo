date格式
%Y YYYY格式的年份
%m mm格式的月份
%d dd格式的日期
%H HH格式的小时数
%M MM格式的分钟数
%S SS格式的秒数
%F YYYY-mm-dd格式的完整日期 同%Y-%m-%d（年月日）
%T HH-MM-SS格式的时间  同%H:%M:%S(时分秒)
%s 时间戳格式（1682249468）
%w 星期几（0-6） 0表示星期天
%u 星期几（1-7） 7表示星期天

netstat和ss命令区别
netstat常用参数：-anplt
-a 显示所有活动的连接以及本机侦听的TCP、UDP端口
-l 显示监听的server port
-n 直接使用IP地址，不通过域名服务器
-p 正在使用socket的程序PID和程序名称
-r 显示路由表
-t 显示TCP传输协议的连线状况
-u 显示UDP传输协议的连线状况
-w 显示RAW传输协议的连线状况

ss(socket statistics)
常用参数：-anplt
-a 显示所有的sockets
-l 显示正在监听的
-n 显示数字IP和端口，不通过域名服务器
-p 显示使用socket的对应的程序
-t 只显示TCP sockets
-u 只显示UDP sockets
-4 -6 只显示v4或v6版本的sockets
-r 尝试进行域名解析，地址/端口
-0	显示PACKET sockets
-w	只显示RAW sockets
-x 只显示UNIX域的sockets

练习：
1. echo $(($RANDOM%5+3)) | md5sum | cut -c 1-8
	生成随机数	$RANDOM       	#0-32767
	如想得到6-87范围内的数：$(($RANDOM%82+6))
	cut 切割命令（得出的md5值切割显示出前8位）
	
	